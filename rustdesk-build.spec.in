%bcond_with flutter
%global toolchain clang
%global debug_package %{nil}
%global __python %{__python3}
%define _lto_cflags %{nil}

%global vcpkg_version 2022.05.10
%global flutter_version 3.0.5

Name:		rustdesk
Version:	@VERSION@
Release:	1%{?dist}
Summary:	Small Gotify daemon to receive messages and forward them as desktop notifications.

License:	GPL
URL:		https://github.com/rustdesk/%{name}
#Source0:	https://github.com/rustdesk/%{name}/archive/%{version}.tar.gz
Source0:	https://github.com/rustdesk/%{name}/archive/refs/heads/master.tar.gz#/rustdesk-master.tar.gz
#Source0:	https://github.com/rustdesk/%{name}/archive/refs/tags/nightly.tar.gz#/rustdesk-nightly.tar.gz
%if %{with flutter}
Source1:	https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_%{flutter_version}-stable.tar.xz
Source2:	https://github.com/SoLongAndThanksForAllThePizza/flutter_rust_bridge/archive/refs/heads/master.tar.gz#/flutter_rust_bridge.tar.gz
%else
Source1:	https://raw.githubusercontent.com/c-smile/sciter-sdk/master/bin.lnx/x64/libsciter-gtk.so
%endif
Source3:	https://github.com/microsoft/vcpkg/archive/refs/tags/%{vcpkg_version}.tar.gz#/vcpkg-%{vcpkg_version}.tar.gz

BuildRequires: rust
BuildRequires: cargo
BuildRequires: rust-packaging >= 21
BuildRequires: rustfmt
BuildRequires: gcc-c++ git curl wget nasm yasm gcc gtk3-devel clang libxcb-devel libxdo-devel libXfixes-devel pulseaudio-libs-devel cmake alsa-lib-devel vcpkg
BuildRequires: clang-devel
BuildRequires: python3-devel
BuildRequires: libvpx-devel
BuildRequires: libyuv-devel
BuildRequires: gstreamer1-devel
BuildRequires: gstreamer1-plugins-base-devel
BuildRequires: libzstd-devel
BuildRequires: libsodium-devel
BuildRequires: libappindicator-gtk3-devel
BuildRequires: opus-devel
BuildRequires: libva-devel
BuildRequires: libvdpau-devel
BuildRequires: openssl-devel
Requires: gtk3 libxcb libxdo libXfixes pulseaudio-libs alsa-lib cjkuni-uming-fonts curl
#Requires: python3-pip
Requires: libvpx
Requires: libyuv
Requires: opus
Requires: gstreamer1
Requires: gstreamer1-plugins-base
Requires: libzstd
Requires: libsodium
Requires: libappindicator-gtk3
Requires: libva
Requires: libvdpau
Requires: openssl

%description
The best open-source remote desktop client software, written in Rust.

%prep
%autosetup -n %{name}-master
%if %{with flutter}
tar -xJf "%SOURCE1" -C ..
tar -xzf "%SOURCE2" -C ..
%endif
tar -xzf "%SOURCE3" -C ..
mkdir -p ../.cargo
#% cargo_prep

cp res/rustdesk.desktop .
sed '/Icon=/s/\/.*\///' -i rustdesk.desktop
sed 's;/lib/;/%{_lib}/;' -i src/ui.rs

#% generate_buildrequires
#% cargo_generate_buildrequires

%build
pushd ..
%if %{with flutter}
export PATH="$RPM_BUILD_DIR/flutter/bin:$RPM_BUILD_DIR/.cargo/bin:$PATH"
dart --disable-analytics
flutter config --no-analytics

dart pub global activate ffigen --version 5.0.1

pushd flutter_rust_bridge-master/frb_codegen
ln -sf ../../.cargo
%cargo_install -- --force
popd
%endif

pushd vcpkg-%{vcpkg_version}
export VCPKG_ROOT=$PWD
export VCPKG_FORCE_SYSTEM_BINARIES=1
./bootstrap-vcpkg.sh -disableMetrics
./vcpkg --disable-metrics install libvpx libyuv opus

#pushd buildtrees/libvpx/src
#pushd *
#./configure
#sed -i 's/CFLAGS+=-I/CFLAGS+=-fPIC -I/g' Makefile
#sed -i 's/CXXFLAGS+=-I/CXXFLAGS+=-fPIC -I/g' Makefile
#%_make
#cp libvpx.a $VCPKG_ROOT/installed/x64-linux/lib/
#popd
#popd

popd
popd

if [ ! -L .cargo ]; then
    cp -a .cargo/. ../.cargo/
    rm -rf .cargo/
    ln -sf ../.cargo
fi

%if %{with flutter}
pushd flutter
flutter pub get
popd

%cargo_build -f hwcodec,flutter -- --lib

flutter_rust_bridge_codegen --rust-input src/flutter_ffi.rs --dart-output flutter/lib/generated_bridge.dart
sed -i "s/ffi.NativeFunction<ffi.Bool Function(DartPort/ffi.NativeFunction<ffi.Uint8 Function(DartPort/g" flutter/lib/generated_bridge.dart

pushd flutter
flutter build linux --release
popd
%else
%__python res/inline-sciter.py
%cargo_build -f inline,hwcodec
%endif

%install
install -Dm 755 target/release/rustdesk %{buildroot}/%{_bindir}/rustdesk
install -Dm 644 %{SOURCE1} %{buildroot}/%{_libdir}/rustdesk/libsciter-gtk.so
install -Dm 644 res/rustdesk.service %{buildroot}/%{_unitdir}/rustdesk.service
install -Dm 644 res/32x32.png %{buildroot}/%{_datadir}/icons/hicolor/32x32/apps/rustdesk.png
install -Dm 644 res/128x128.png %{buildroot}/%{_datadir}/icons/hicolor/128x128/apps/rustdesk.png
install -Dm 644 res/128x128@2x.png %{buildroot}/%{_datadir}/icons/hicolor/128x128@2/apps/rustdesk.png
install -Dm 644 res/logo.svg %{buildroot}/%{_datadir}/icons/hicolor/scalable/apps/rustdesk.svg
install -Dm 644 rustdesk.desktop %{buildroot}/%{_datadir}/applications/rustdesk.desktop

%files
%{_bindir}/rustdesk
%dir %{_libdir}/rustdesk
%{_libdir}/rustdesk/libsciter-gtk.so
%{_unitdir}/rustdesk.service
%{_datadir}/applications/rustdesk.desktop
%{_datadir}/icons/hicolor/32x32/apps/rustdesk.png
%{_datadir}/icons/hicolor/128x128/apps/rustdesk.png
%{_datadir}/icons/hicolor/128x128@2/apps/rustdesk.png
%{_datadir}/icons/hicolor/scalable/apps/rustdesk.svg

#% pre
#% sysusers_create_compat rustdesk.service

%post
%systemd_post rustdesk.service

%postun
%systemd_postun_with_restart rustdesk.service

